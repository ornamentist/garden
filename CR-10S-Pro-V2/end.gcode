;;
;; End G-code for a Creality CR-10S Pro V2 and Prusa slicer.
;;
;; References:
;;
;; * Prusa slicer G-code for the CR-10S Pro V2
;;
;; * https://www.youtube.com/watch?v=nsRntOH_DdQ
;;

; Move the print head up if needed.
{if max_layer_z < max_print_height}
  G1 Z{z_offset + min(max_layer_z + 2, max_print_height)} F600
{endif}

; Present the print.
G1 X5 Y250 F{travel_speed*60}

; Move the print head further up.
{if max_layer_z < max_print_height-10}
  G1 Z{z_offset + min(max_layer_z + 70, max_print_height - 10)} F600
{endif}

; Turn the heatbed off
M140 S0

; Turn the hot-end off
M104 S0

; Turn the fan off
M107

; Turn motors off
M84 X Y E

M117 Completed print.
