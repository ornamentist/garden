;;
;; Start G-code for a Creality CR-10S Pro V2 and Prusa slicer.
;;
;; References:
;;
;; * Prusa slicer G-code for the CR-10S Pro V2
;;
;; * https://www.youtube.com/watch?v=nsRntOH_DdQ
;;

M117 Preparing print...

; Use units in millimeters
G21

; Use absolute coordinates
G90

; Use extruder relative positioning
M83

; Set bed levelling nozzle temperature
M104 S190

; Set the heated bed temperature
M140 S[first_layer_bed_temperature]

; Wait for the heated bed to reach temperature
M190 S[first_layer_bed_temperature]

; Wait for the nozzle to reach levelling temperature
M109 R190

M117 Run auto bed levelling ...

; Do X levelling
G34

; Home all axes (required before a G29)
G28

; Run an auto-bed level
G29

; Save and use the bed levelling mesh
M420 S1


M117 Print purge lines...

; Set nozzle printing temperature
M104 S[first_layer_temperature]

; Wait for nozzle printing temperature
M109 R[first_layer_temperature]

; Reset extruder position
G92 E0

; Move the Z axis up
G1 Z50 F240

; Small filament retraction to avoid dragging
G92 E-2

; Move to the purge line start position
G1 X3 Y12 F3000

; Set the Z axis offset
G1 Z0.28 F240

; Print the first purge line
G1 Y280 E15 F1500

; Move a little in the +X direction
G1 X6 F5000

; Reset the extruder position
G92 E0

; Print the second purge line
G1 Y12 E15 F1200

; Reset the extruder position
G92 E0

M117 Starting print...
