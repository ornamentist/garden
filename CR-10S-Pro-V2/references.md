# Creality CR-10S Pro V2 Useful References


<!-- -- -->
## Introduction

Here's a collection of references for calibration and enclosures for the
[Creality CR-10S Pro V2][CR-10S-Pro-V2] printer. References are given in
no particular order and are of varying quality and usefulness.



<!-- -- -->
## License

This document and accompanying files are licensed under a [Creative
Commons Attribution 4.0 International License][CC-license].



<!-- -- -->
## Calibration references

* <https://www.th3dstudio.com/product/ezlevel-z-axis-stickers/>

* <https://www.reddit.com/r/CR10/comments/a3ben5/cr10s_horizontal_bar_not_level_or_straight_how_to/>

* <https://www.thingiverse.com/groups/creality-cr10s-pro/forums/general/topic:46147>

* <https://www.youtube.com/watch?v=hitJUx3eKW8>

* <https://www.youtube.com/watch?v=gAJRw1X738s>

* <https://www.creality.com/download>

* <https://www.thingiverse.com/thing:3905999>

* <https://3dprintbeginner.com/cr10s-pro-prusa-slicer-profile/>

* <https://3dprintbeginner.com/extruder-calibration-guide/>

* <https://3dprintbeginner.com/3d-printer-calibration-guide-using-ideamaker/>


## Enclosure references

* <https://www.reddit.com/r/3Dprinting/comments/nfibxk/it_aint_much_but_this_enclosure_took_three_months/>

* <https://www.reddit.com/r/CR10/comments/nhzkjb/project_enclosure_for_cr10v3_finished_fans/>

* <https://www.etsy.com/listing/691578548/3d-printer-enclosure-metal-frame>

* <https://www.etsy.com/au/listing/769793038/3d-printer-enclosure-wood-frame>

* <https://www.etsy.com/au/listing/986311917/3d-printer-enclosure-xl-wood-frame>

* <https://www.blvprojects.com/post/blv-enclosure-progress-update>

* <https://back7.co/home/learning-to-scale-small-manufacturing-with-enclosures-amp-big-blue-saw>

* <https://back7.co/home/tutorial-designing-a-stackable-3d-printer-enclosure-in-tinkercad>

* <https://www.amazon.com/gp/product/B07BMQZZ2M/ref=as_li_ss_tl>

* <https://www.amazon.com/dp/B01H4VY6VG/ref=sspa_dk_detail_1>

* <https://openbuilds.com/builds/openbuilds-enclosure-series.9535/>

* <https://openbuilds.com/threads/acrylic-for-laser-enclosure.16247/>

* <https://forum.lulzbot.com/t/enclosures-and-ventilation-of-the-machine-not-the-print/2146/14>

* <https://ohai.lulzbot.com/project/cat_guard/accessories/>

* <https://www.reddit.com/r/prusa3d/comments/bd6grk/aluminium_3d_printer_enclosure_for_mk3_diy_gallery/>

* <https://imgur.com/gallery/LmXUDYS>


<!-- Local references -->

[CR-10S-Pro-V2]: https://www.creality3dofficial.com/products/cr-10s-pro-v2-3d-printer

[CC-license]: http://creativecommons.org/licenses/by/4.0/

