# A Digital Garden

This repository is a collection of notes, ideas, experiments and HOW-TO
documents that form a [digital garden][garden-history] of sorts.

Here's some references that explain the ideas behind digital gardens:
[1][garden-1], [2][garden-2], [3][garden-3], [4][garden-history].


## Contents

The sub-directories of this repository are organized as follows:

| Subject                                   |
| ----------------------------------------- |
| [CR-10S-Pro-V2 3D printer][CR-10S-Pro-V2] |
| [Common Lisp and Coalton][common-lisp]    |



<!-- References -->

[garden-history]: https://maggieappleton.com/garden-history

[garden-1]: https://garden.anthonyamar.fr/Digital+garden/Digital+garden

[garden-2]: https://www.conordewey.com/blog/on-digital-gardening/

[garden-3]: https://www.connectionsbyfinsa.com/digital-gardens/?lang=en

[CR-10S-Pro-V2]: https://gitlab.com/ornamentist/garden/-/tree/main/CR-10S-Pro-V2

[common-lisp]: https://gitlab.com/ornamentist/garden/-/tree/main/common-lisp
