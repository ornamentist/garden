# Coalton Language How-To

## Introduction

This document is a collection of notes and snippets for working with the
[Coalton][coalton] language in a [Common Lisp][common-lisp] environment.


## Status

These notes are at an early stage and are very incomplete.


## Notation

In the snippets that follow lines starting with a `$` character are shell
commands and lines starting with a `*` character are Common Lisp REPL
(here [SBCL][SBCL]) commands. Command output is not shown.


## Coalton How-Tos

### Installing Coalton

To use Coalton we'll need a Common Lisp compiler (here I'm using [SBCL][SBCL])
and a [Quicklisp][quicklisp] installation.

Follow the installation instructions for SBCL and then the Quicklisp
installation instructions, including updating the `${HOME}/.sbclrc`
configuration file to auto-load Quicklisp.

Coalton is not (yet) registered with [Quicklisp][quicklisp] so it must be
cloned into a directory that Quicklisp will recognize, e.g.
`${HOME}/quicklisp/local-projects`:

```sh
$ cd ${HOME}/quicklisp/local-projects

$ git clone git@github.com:coalton-lang/coalton.git
```

Then it can be loaded by Quicklisp:

```lisp
* (ql:quickload :coalton)
```

To run the project tests with the `coalton/tests` project:

```lisp
* (ql:quickload :coalton/tests)

* (asdf:test-system :coalton)
```


## Installing Coalton's `big-float` library

Coalton's `Big-Float` type relies on the Gnu MPFR library being installed and
accessible. Because of this it's installed in a separate Coalton system.

To load the `big-float` library and create a `Big-Float` value:

```lisp
(ql:quickload :coalton/library/big-float)

(in-package :coalton-user)

(coalton (the coalton-library/big-float:Big-Float (into (the Single-Float math:nan))))
```


## Use Coalton in a project

To use Coalton in a Common Lisp project, add `#:coalton` to your ASDF project
file `:depends-on` list:

```lisp
(defsystem "my-project"
  :pathname "src/"

  :serial t

  :depends-on
    ("coalton"
     "coalton-library")

  :components
    (...))
```

## Coalton package and system conventions

The examples above follow the Common Lisp conventions around naming in systems
and packages: use symbols for package names and strings for ASDF component
names. For example, `:depends-on ... "coalton"` and `defsystem "my-project"`.


<!-- References used above -->

[coalton]: https://coalton-lang.github.io/

[common-lisp]: https://lisp-lang.org/

[quicklisp]: https://www.quicklisp.org/beta/

[ASDF]: https://asdf.common-lisp.dev/

[SBCL]: http://www.sbcl.org/

[rlwrap]: https://github.com/hanslub42/rlwrap
