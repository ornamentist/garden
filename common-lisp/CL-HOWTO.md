# Common Lisp How-To


## Introduction

This document is a collection of shell and Lisp snippets for working in the
[Common Lisp][common-lisp] ecosystem.


## Status

These notes are at an early stage and are very incomplete.


## Overview

### Compilers

As with other programming languages, Common Lisp source code needs to be
processed by a translator of some kind, e.g. a _compiler_ or _interpreter_.
There are [a number][compilers] of these available, both open source and
commercial, across multiple platforms and with different strengths and
weaknesses.

[Steel Bank Common Lisp (SBCL)][SBCL] is a popular compiler used in the
examples in these notes.


### Build systems

A _build_ _system_ for Common Lisp loads _components_ _metadata_ and system
_dependencies_. The build system for Common Lisp is [ASDF][ASDF].

ASDF artifacts are specified in _system_ _definitions_. The _product_ of an
ASDF system build system could, for example, be an _application_ or a
_library_.

ASDF commands are accessible in the Common Lisp REPL.


## Library managers

A _library_ _manager_ manages Common Lisp _libraries_ in a _repository_. The
de-facto library manager for Common Lisp is [Quicklisp][quicklisp]. Quicklisp
can use ASDF to analyze libraries and find their dependencies.

Quicklisp is also accessible from the Common Lisp REPL.


## Notation

In the snippets that follow lines starting with a `$` character are shell
commands and lines starting with a `*` character are Common Lisp REPL
(here [SBCL][SBCL]) commands. Command output is not shown.


## Quicklisp How-Tos

### Install Quicklisp

[Quicklisp][quicklisp] is the the de-facto library manager for Common Lisp.
It's installed by accessing a (Lisp) download and loading it in a Common Lisp
REPL (here provided by [SBCL][SBCL]):

```sh
$ curl -O https://beta.quicklisp.org/quicklisp.lisp.asc

$ rlwrap sbcl --load quicklisp.lisp

* (quicklisp-quickstart:install)
```

Here I'm using the [rlwrap][rlwrap] utility to provide command history and
editing to the SBCL Common Lisp REPL.


### Load Quicklisp

The Quicklisp installer creates a setup (Common Lisp) script that needs to be
loaded every time by the Common Lisp REPL (here SBCL):

```sh
$ rlwrap sbcl --load ~/quicklisp/setup.lisp
```

To have this setup script loaded automatically in a Common Lisp REPL use this
Quicklisp command to add setup code to the REPL initialization file(s):

```lisp
* (ql:add-to-init-file)
```

For SBCL this will update `~/.sbclrc`


### Find libraries registered with Quicklisp

To search Quicklisp for registered libraries:

```lisp
* (ql:system-apropos "search term")
```


### Install a library from Quicklisp

To install a library registered with Quicklisp:

```lisp
* (ql:quickload "library name")
```


### Update all Quicklisp managed libraries

To update all Quicklisp installed libraries:

```lisp
* (ql:update-dist "quicklisp")
```


### Uninstall a library from Quicklisp

To uninstall a Quicklisp registered library:

```lisp
* (ql:uninstall "library name")
```


### Update the Quicklisp client

To update the Quicklisp client installation:

```lisp
* (ql:update-client)
```


### Find library dependencies in Quicklisp

To check Quicklisp for dependencies on a library:

```lisp
* (ql:who-depends-on :library-name)
```


### Load a local library into Quicklisp

To load a local library that's not registered with Quicklisp put the library
source tree into the Quicklisp `local-projects` directory:

```sh
$ cd ~/quicklisp/local-projects/

$ # Git clone or otherwise copy library-name source tree
```

Then in a Common Lisp REPL:

```lisp
* (ql:quickload "library-name")
```


### Load an ASDF registered system with Quicklisp

Common Lisp installations use a standard build system: [ASDF][ASDF]. For an
ASDF system file `my-project.asd` at the path `/path/my-project` use:

```lisp
* (push #p"/path/my-project/" asdf:*central-registry*)

* (ql:quickload "my-project")
```

This will also automatically install Quicklisp packages that `my-project`
depends on.


### Clean up libraries in Quicklisp

To remove old and unused libraries managed by Quicklisp:

```lisp
* (ql-dist:clean (ql-dist:dist "quicklisp"))
```


### Clean the Common Lisp compiler cache

Common Lisp compilers (like SBCL) and Quicklisp cache source files in a
standard location: `$(HOME)/.cache/common-lisp`. This cache can be cleaned
simply by deleting the directory:

```sh
$ rm -rf $(HOME)/.cache/common-lisp
```


## ASDF How-Tos

### Locating ASDF systems

ASDF needs to know where to find _system_ _definitions_. By convention it uses
`${HOME}/.config/common-lisp/source-registry.conf.d` in files `*.conf`. An
example `my-projects.conf` might contain:

```lisp
(:tree "${HOME}/my-projects")
```

Which tells ASDF to (also) look through the directory tree
`${HOME}/my-projects` for system definitions.


### ASDF system conventions

ASDF defines a _primary_ _system_ that can be found in the _source_ _registry_
in an `.asd` file with the same name as the primary system--that is the
my-project primary system has a `my-project.asd` file in the source registry.

Primary system names should only be lowercase ASCII, digits or the characters
`-` (hyphen) and `.` (dot). The (dot) character can be used to create an
informal hierarchy of sub-systems, for example, `my-project.gui`,
`my-project.dbms`, `my-project.examples`.

ASDF also defines a _secondary_ _system_ to be a system defined in the same
`.asd` file as a primary system, but with a `/` based naming convention. For
example `my-project/tests`. This convention allows ASDF to find secondary
systems based on the name of the parent primary system.


### Check whether ASDF is installed

To check whether a version of ASDF is installed in a Lisp environment:

```lisp
* (asdf:asdf-version)
```

If this value is not defined, the recommended way to load ASDF is:

```lisp
* (require "asdf")
```


### Load an ASDF system

Load an ASDF system definition with:

```lisp
* (asdf:load-system "my-project")
```

Note that system names like `my-project` should be lowercase strings of
alphanumeric and optional {`-`, `.`} characters.


### Build an ASDF system

Build a system definition (including any system `make` customization) with:

```lisp
* (asdf:make "my-project")
```


### Run an ASDF system tests

If a project has a system definition for tests, they can be loaded and run
with:

```lisp
* (asdf:test-system "my-project")

```


### Reload an ASDF system

To reload a system in a running REPL session (after changing the system `.asd`
file for example):

```lisp
* (load #P"/path/to/my-project.asd")
```


### Inspect an ASDF system

To see what details ASDF has for a system use:

```lisp
* (describe (asdf:find-system "my-project"))
```


### See which ASDF systems are loaded

To see which ASDF systems have already been loaded by ASDF:

```lisp
* (asdf:already-loaded-systems)
```


### Find the `.asd` file for an ASDF system

To see which `.asd` file ASDF associates with a system:

```lisp
* (asdf:system-source-file "my-project")
```


### Reset the ASDF configuration

To reset any changed ASDF configuration:

```lisp
* (asdf:clear-configuration)
```


## Packages

### List all the symbols in a package

To list all the symbols in a Common Lisp package:

```lisp
(do-external-symbols (symbol (find-package "<PACKAGE NAME>")) (print symbol))
```


<!-- References used above -->

[ASDF]: https://asdf.common-lisp.dev/

[Coalton]: https://coalton-lang.github.io/

[common-lisp]: https://lisp-lang.org/

[compilers]: https://common-lisp.net/implementations

[quicklisp]: https://www.quicklisp.org/beta/

[rlwrap]: https://github.com/hanslub42/rlwrap

[SBCL]: http://www.sbcl.org/

[REPL]: https://lisp-lang.org/learn/getting-started/
